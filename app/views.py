from django.shortcuts import render,redirect
from .forms import studentsform
from .models import Students
# Create your views here.

def Insert(req):
    if req.method == 'POST':
        form = studentsform(req.POST)
        if form.is_valid():
            form.save()
            return redirect('create')
    else:
        form = studentsform()

    student = Students.objects.all()
    return render(req, 'home.html',{'form':form,'student':student})



def edit(req, stdid):
    student = Students.objects.get(stdid=stdid)
    form = studentsform(instance= student)
    if req.method == 'POST':
        form = studentsform(req.POST,instance= student)
        if form.is_valid():
            print('stdid 2',stdid)
            form.save()
            return redirect('create')
    return render(req, 'edit.html',{'form':form,'student':student})



def delete(req, stdid):
     student = Students.objects.get(stdid=stdid)
     student.delete()
     return redirect('create')
