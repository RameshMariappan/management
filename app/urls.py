from django.urls import path
from . import views
urlpatterns=[
    path('',views.Insert,name='create'),
    path('edit/<int:stdid>/',views.edit,name='edit'),
    path('delete/<int:stdid>/',views.delete,name='delete'),

]
